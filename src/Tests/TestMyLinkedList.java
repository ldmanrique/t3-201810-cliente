package Tests;


import static org.junit.Assert.*;


import org.junit.Before;
import org.junit.Test;

import model.data_structures.MyLinkedList;

public class TestMyLinkedList {

	private MyLinkedList<Integer> lista;


	@Before
	public void setupEscenario1( )
	{

		lista = new MyLinkedList<Integer>();
		lista.add(5);
		lista.add(8);
		lista.add(10);

	}



	// -----------------------------------------------------------------
	// M�todos de prueba
	// -----------------------------------------------------------------

	/**
	 * add
	 * remove
	 * get
	 * size
	 * get by position
	 * listing
	 * getCurrent
	 * next
	 */
	@Test
	public void testAdd()
	{
		lista.add(6);
		assertEquals( "El tama�o de la lista no es correcto", lista.size(), 4 );

	}

	@Test
	public void testRemove()
	{


		assertTrue("No est� eliminando" , lista.remove(5));
	}

	@Test
	public void testGet()
	{

		assertNotNull( "El elemento no es el deseado",(Integer) lista.get(5) );

	}

	@Test
	public void testSize()
	{
		lista.add(9);	
		assertEquals( "Eltama�o no es correcto", lista.size(), 4 );

	}

	@Test
	public void TestGetI()
	{

		assertNotNull("El elemento no es el deseado", lista.getI(0) );
	}

	@Test
	public void TestgetCurrent()
	{

		assertNotNull( "El elemento no es el deseado", lista.getI(0) );

	}


}
