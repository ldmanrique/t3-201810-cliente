package model.vo;

/**
 * Representation of a Service object
 */
public class Servicio implements Comparable<Servicio>{	
	private String taxiId;
	private String tripId;
	private int tripSecons;
	private double tripMiles;
	
	public Servicio(String pTaxiId,String pTripId,int pTripSecons,double pTripMiles ) {
		taxiId =pTaxiId;
		tripId=pTripId;
		tripSecons= pTripSecons;
		tripMiles= pTripMiles;
	}
	/**
	 * @return id - Trip_id
	 */
	public String getTripId()
	{
		// TODO Auto-generated method stub
		return tripId;
		
	}	
	
	/**
	 * @return id - Taxi_id
	 */
	public String getTaxiId() {
		// TODO Auto-generated method stub
	
		return taxiId;
		
	}	
	
	/**
	 * @return time - Time of the trip in seconds.
	 */
	public int getTripSeconds() {
		// TODO Auto-generated method stub
		return tripSecons;
	}

	/**
	 * @return miles - Distance of the trip in miles.
	 */
	public double getTripMiles() {
		// TODO Auto-generated method stub
		return tripMiles;
		}

	
	@Override
	public int compareTo(Servicio arg0) {
		// TODO Auto-generated method stub
		return 0;
	}
	}