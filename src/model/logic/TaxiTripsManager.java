package model.logic;

import java.io.FileReader;
import java.util.ArrayList;
import java.util.Iterator;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;

import api.ITaxiTripsManager;

import model.data_structures.IQueue;
import model.data_structures.IStack;
import model.data_structures.LinkedList;
import model.data_structures.MyLinkedList;
import model.vo.Compania;
import model.vo.CompaniaServicios;
import model.vo.CompaniaTaxi;
import model.vo.FechaServicios;
import model.vo.InfoTaxiRango;
import model.vo.RangoDistancia;
import model.vo.RangoFechaHora;

import model.vo.Servicio;
import model.vo.ServiciosValorPagado;
import model.vo.Taxi;
import model.vo.ZonaServicios;

public class TaxiTripsManager implements ITaxiTripsManager 
{
	// TODO
		// Definition of data model 
		private MyLinkedList<Servicio> services;
		private MyLinkedList<Taxi> taxis;
		private MyLinkedList<String> ids;
		private MyLinkedList<CompaniaServicios> compa�iaServicio;
		private MyLinkedList<Compania> compa�ia;
		private MyLinkedList<CompaniaTaxi> compa�iaTaxi;
		private MyLinkedList<FechaServicios> fechaServicios;
		private MyLinkedList<InfoTaxiRango> infoTaxiRango;
		private MyLinkedList<RangoDistancia> rangoDistancia;
		private MyLinkedList<RangoFechaHora> rangoFechaHora;
		private MyLinkedList<ServiciosValorPagado> serviciosValorPagado;
		private MyLinkedList<ZonaServicios> zonaServicio;
		
		

		
	public static final String DIRECCION_SMALL_JSON = "./data/taxi-trips-wrvz-psew-subset-small.json";
	public static final String DIRECCION_MEDIUM_JSON = "./data/taxi-trips-wrvz-psew-subset-medium.json";
	public static final String DIRECCION_LARGE_JSON = "./data/taxi-trips-wrvz-psew-subset-large.json";
	
	public void loadServices (String serviceFile) {
		// TODO Auto-generated method stub

		MyLinkedList<Servicio> services= new  MyLinkedList <Servicio>();
		MyLinkedList <Taxi> taxis= new MyLinkedList<Taxi>();
		ArrayList<String> ids= new ArrayList();
		JSONParser parser =new JSONParser();
		try
		{
			JSONArray listaGson= (JSONArray)parser.parse(new FileReader(serviceFile));
			Iterator iter=  listaGson.iterator();
			int x= 0;

			while(iter.hasNext())
			{
				JSONObject o=(JSONObject) listaGson.get(x);

				String company= "Independents";
				if(o.get("company") != null) {
					company= (String) o.get("company");
				}
				int community =0;
				if(o.get("dropoff_community_area") != null) {
					community= Integer.parseInt((String) o.get("dropoff_community_area"));
				}
				
				String tripId= (String) o.get("trip_id");
				String taxiId= (String) o.get("taxi_id");
				int tripSeconds= Integer.parseInt((String) o.get("trip_seconds"));
				double tripMiles= Double.parseDouble((String) o.get("trip_miles"));
				double tripTotal= Double.parseDouble((String) o.get("trip_total"));

				Servicio serviciox = new Servicio(taxiId, tripId, tripSeconds, tripMiles);
				services.add(serviciox);
				Taxi taxix= new Taxi(company, taxiId);
				if(!ids.contains(taxiId))
				{
					taxis.add(taxix);
					ids.add(taxix.getTaxiId());
				
				

				}
				System.out.println(serviciox);
				x++;
				
				iter.next();
				
			}

			System.out.println("La cantidad de servicios cargados fue: " + services.size());
			System.out.println("Se cargaron "+ taxis.size()+ " taxis");



		}

		catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}

	}
	 /**
     *  <br>retorna los servicios en un rango de tiempo
     * <b> post: </b> retorna una cola en orden cronologico con servicios de taxi que comenzaron y terminaron dentro del tiempo pasado por parametro .
     * @param rango = rango de fecha y hora en el cual se hara la busqueda.
     */
	@Override //1A
	public IQueue <Servicio> darServiciosEnPeriodo(RangoFechaHora rango)
	{
		// TODO Auto-generated method stub
		return null;
	}
	/**
     *  <br>devuelve el taxi con mas carreraas iniciadas en un rango de tiempo dado, de una compa�ia especifica
     * <b> post: </b> devuelve el taxi de la compa�ia dada con mas servicios iniciados en el rango de tiempo.
     * @param copany = nombre de la compa�ia .
     * @param rango = rango de fecha y hora en el que se va a realizar la busqueda.
     */

	@Override //2A
	public Taxi darTaxiConMasServiciosEnCompaniaYRango(RangoFechaHora rango, String company)
	{
		// TODO Auto-generated method stub
		return null;
	}
	/**
     *  <br>busca la informacion completa de un taxi 
     * <b> post: </b> devuelve un objeto de tipo infoTaxiRango, con la informacion del taxi, nombre de la compa�ia, valor total ganado,numero  servicios prestados,distancia recorrida, tiempo total de servicio .
     * @param id = id del taxi buscado .
     * @param rango = rango de fecha y hora en el que se va a realizar la busqueda.
     */
	@Override //3A
	public InfoTaxiRango darInformacionTaxiEnRango(String id, RangoFechaHora rango)
	{
		// TODO Auto-generated method stub
		return null;
	}
	/**
      * <b> post:lista de listas ordenadas cronologicamente con todos los servicios de todas las compa�ias
      * </b>  devuelve una lista de los rangos de distancias recorridos, en la que se encuentran todos los servicios 
     * de taxis por compa�ia, en un rango de tiempo  especifico, ordenada por distancia recorrida.
     * @param fecha = fecha donde se realizara la consultA .
     * @param hora inicial=hora inicial del rango de la consulta.
     *  @param hora final=hora final del rango de la consulta. 
     */

	@Override //4A
	public LinkedList<RangoDistancia> darListaRangosDistancia(String fecha, String horaInicial, String horaFinal) 
	{
		// TODO Auto-generated method stub
		return null;
	} 
	/**                                                                                                                       
    *  <br> busca el taxi con mayor facturacion en un periodo de tiempo en una comp                                                                                                              
	* <b> post: </b> taxi con mayor facturacion en la compa�ia dada
    * @param rango = objeto de tipo RangoFecha , con la hora y fecha inicial y final donde se realizara la busqueda .                                                                  
	* @param nomCompania= nombre de la compa�ia del taxi.                                                          
    */                                                                                                                       

	
	@Override //2B
	public Taxi darTaxiMayorFacturacion(RangoFechaHora rango, String nomCompania) 
	{
		// TODO Auto-generated method stub
		return null;
	}

	
	
	
	
	@Override //3B
	public ServiciosValorPagado[] darServiciosZonaValorTotal(RangoFechaHora rango, String idZona)
	{
		// TODO Auto-generated method stub
		return null;
	}
	/**                                                                                                                                                                                          
     *  <br> busca la informacion completa de una zona de la ciudad en un periodo de tiempo                                                                                                              
	 * <b> post: </b> lista de objetos tipo Zona de servicios con: id de la zona y fecha de servicio                                                                                                                              
	 * @param rango = objeto de tipo RangoFecha , con la hora y fecha inicial y final donde se realizara la busqueda .                                                                            
                                                                                                                                        	 */                                                                                                                                                                                           
	
	@Override //4B
	public LinkedList<ZonaServicios> darZonasServicios(RangoFechaHora rango)
	{
		// TODO Auto-generated method stub
		return null;
	}
	/**                                                                                                                                          
      *  <br> identifica el top  n de compa�ias que masservicios iniaron en un periodo de tiempo                                                       
	  * <b> post: </b> lista ordenada  de n compa�ias  de mayor a menor por numero de servicios iniciados                                           
	  * @param rango = objeto de tipo RangoFecha , con la hora y fecha inicial y final donde se realizara la busqueda .
	  *  @param n = numero del rango de consulta                       
	                                                                                                                                     	 */  
	@Override //2C
	public LinkedList<CompaniaServicios> companiasMasServicios(RangoFechaHora rango, int n)
	{
		// TODO Auto-generated method stub
		return null;
	}
	/**                                                                                                                         
       *  <br> busca el taxi mas rentable de cada compa�ia es decir, el taxi culla rellacion de dinero ganado por distancia rcrrida es mayor                              
	   * <b> post: </b> lista con los taxis mas rentables de las compa�ias                       
	   */
	
	                                                                                                                             
	@Override //3C
	public LinkedList<CompaniaTaxi> taxisMasRentables()
	{
		// TODO Auto-generated method stub
		return null;
	}

	@Override //4C
	public IStack <Servicio> darServicioResumen(String taxiId, String horaInicial, String horaFinal, String fecha) 
	{
		// TODO Auto-generated method stub
		return null;
	}
	
	@Override
	public LinkedList<Compania> darCompaniasTaxisInscritos() {
		// TODO Auto-generated method stub
		return null;
	
	 }


	

}
